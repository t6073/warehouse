package pl.oskarskalski.warehouse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.oskarskalski.warehouse.model.EmployeeRole;
import pl.oskarskalski.warehouse.repository.EmployeeRoleRepository;

import java.util.List;

@Service
public class EmployeeRoleService {
    private final EmployeeRoleRepository employeeRoleRepository;

    @Autowired
    public EmployeeRoleService(EmployeeRoleRepository employeeRoleRepository) {
        this.employeeRoleRepository = employeeRoleRepository;
    }

    public void addEmployeeRole(EmployeeRole employeeRole) {
        employeeRoleRepository.save(employeeRole);
    }

    public List<EmployeeRole> getEmployeeRolesByEmployeeId(long id){
        return employeeRoleRepository.findAllByEmployeeId(id);
    }

    public List<EmployeeRole> getEmployeeRolesByRoleId(long id){
        return employeeRoleRepository.findAllByRoleId(id);
    }
}
