package pl.oskarskalski.warehouse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.oskarskalski.warehouse.model.Item;
import pl.oskarskalski.warehouse.repository.ItemRepository;

import java.util.*;

@Service
public class ItemService {
    private final ItemRepository itemRepository;

    @Autowired
    public ItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    /**
     * Find an item by his primary key
     *
     * @param uuid
     * @return item object
     */
    public Item getItemById(String uuid) {
        return itemRepository
                .findById(uuid)
                .orElseThrow(NullPointerException::new);
    }

    /**
     * Find items by item's id
     *
     * @param uuid
     * @return List of items
     */
    public List<Item> getItemsByItemId(String uuid) {
        return itemRepository
                .findAllByItemId(uuid);
    }

    /**
     * Find items by theirs item's uuid
     *
     * @param queryString
     * @return Lists of different kind of items
     */
    public List<List<Item>> getItemsByItemsId(String queryString) {
        String[] uuids = null;
        if (queryString == null) {
            uuids = itemRepository.findAllItemIds2();
        }

        if (uuids == null)
            uuids = queryString.split(",");

        List<List<Item>> multiItemLists = new ArrayList<>();
        for (String uuid : uuids) {
            multiItemLists
                    .add(getItemsByItemId(uuid));
        }
        return multiItemLists;
    }

    /**
     * Add item to the repository
     *
     * @param item
     * @return Response message
     */
    public Optional<String> addItem(Item item) {
        String uuid = UUID.randomUUID().toString();
        item.setId(uuid);
        itemRepository.save(item);
        return null;
    }

    /**
     * Soft delete of the item in the repository
     *
     * @param locationId
     * @param itemId
     * @return Response message
     */
    public Optional<String> softDeleteItemById(String locationId, String itemId) {
        List<Item> items = itemRepository.findAllByLocationIdAndItemId(locationId, itemId);
        Item item = items.get(0);
        item.setSoftDeleteStatus(true);
        itemRepository.save(item);
        return null;
    }
}
