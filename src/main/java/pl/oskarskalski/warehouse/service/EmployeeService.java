package pl.oskarskalski.warehouse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.oskarskalski.security.authority.UserAuthorities;
import pl.oskarskalski.security.configuration.JwtConfiguration;
import pl.oskarskalski.security.configuration.PasswordConfiguration;
import pl.oskarskalski.security.model.UsernameAndPasswordAuthenticationRequest;
import pl.oskarskalski.warehouse.model.Employee;
import pl.oskarskalski.warehouse.model.EmployeeRequest;
import pl.oskarskalski.warehouse.model.EmployeeRole;
import pl.oskarskalski.warehouse.model.Password;
import pl.oskarskalski.warehouse.repository.EmployeeRepository;
import pl.oskarskalski.warehouse.repository.EmployeeRoleRepository;
import pl.oskarskalski.warehouse.repository.PasswordRepository;
import pl.oskarskalski.warehouse.repository.RoleRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;
    private final EmployeeRoleRepository employeeRoleRepository;
    private final PasswordRepository passwordRepository;
    private final RoleRepository roleRepository;
    private final PasswordConfiguration passwordConfiguration = new PasswordConfiguration();

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository, EmployeeRoleRepository employeeRoleRepository, PasswordRepository passwordRepository, RoleRepository roleRepository) {
        this.employeeRepository = employeeRepository;
        this.employeeRoleRepository = employeeRoleRepository;
        this.passwordRepository = passwordRepository;
        this.roleRepository = roleRepository;
    }

    public void addEmployee(EmployeeRequest employeeRequest) {
        Employee employee = new Employee();

        employee.setUsername(employeeRequest.getUsername());
        employee.setFirstname(employeeRequest.getFirstname());
        employee.setLastname(employeeRequest.getLastName());

        String uuid = UUID.randomUUID().toString();
        employee.setPasswordId(uuid);

        employeeRepository.save(employee);

        Password password = new Password();
        password.setPasswordId(uuid);
        password.setPassword(passwordConfiguration
                .passwordEncoder()
                .encode(employeeRequest.getPassword()));
        passwordRepository.save(password);
    }

    public void softDeleteEmployee(long id) {
        Employee employee = employeeRepository.findById(id)
                .orElseThrow(NullPointerException::new);

        employee.setSoftDelete(true);
        employeeRepository.save(employee);
    }

    public Employee getEmployeeById(long id) {
        return employeeRepository.findById(id)
                .orElseThrow(NullPointerException::new);
    }

    public ResponseEntity<String> signInTheEmployee(UsernameAndPasswordAuthenticationRequest usernameAndPasswordAuthenticationRequest) {
        Employee employee = employeeRepository
                .findByUsername(usernameAndPasswordAuthenticationRequest.getUsername())
                .orElseThrow(NullPointerException::new);
        Password password = passwordRepository
                .findById(employee.getPasswordId())
                .orElseThrow(NullPointerException::new);
        boolean validPassword = passwordConfiguration.passwordEncoder().matches(usernameAndPasswordAuthenticationRequest.getPassword(), password.getPassword());
        if (validPassword) {
            List<Long> employeeRoles = employeeRoleRepository.findAllByEmployeeId(
                            employee.getId()).stream().map(EmployeeRole::getRoleId)
                    .collect(Collectors.toList());

            List<String> roleNames = new ArrayList<>();

            employeeRoles.forEach(e -> roleNames.add(
                    Objects.requireNonNull(roleRepository.findById(e).orElse(null))
                            .getRoleName()
            ));

            UserAuthorities userAuthorities = new UserAuthorities();
            userAuthorities.setGrantedAuthorities(roleNames);

            JwtConfiguration jwtConfiguration = new JwtConfiguration();
            String jwt = jwtConfiguration.build(usernameAndPasswordAuthenticationRequest, userAuthorities);

            return ResponseEntity.ok()
                    .header("Authorization", "Bearer " + jwt)
                    .body("Response with header using ResponseEntity");
        }
        return ResponseEntity.status(403)
                .body("Wrong password");
    }
}
