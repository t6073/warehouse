package pl.oskarskalski.warehouse.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Item {
    @Id
    private String id;
    private String itemId;
    private String userId;
    private String locationId;
    private Date timeStamp;
    private boolean softDeleteStatus;
}
