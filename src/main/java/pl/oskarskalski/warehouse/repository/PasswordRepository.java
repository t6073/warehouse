package pl.oskarskalski.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.oskarskalski.warehouse.model.Password;

@Repository
public interface PasswordRepository extends JpaRepository<Password, String> {
}
