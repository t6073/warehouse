package pl.oskarskalski.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.oskarskalski.warehouse.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
}
