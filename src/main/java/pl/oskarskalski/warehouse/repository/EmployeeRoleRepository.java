package pl.oskarskalski.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.oskarskalski.warehouse.model.EmployeeRole;
import pl.oskarskalski.warehouse.model.Item;

import java.util.List;

@Repository
public interface EmployeeRoleRepository extends JpaRepository<EmployeeRole, Long> {
    List<EmployeeRole> findAllByEmployeeId(long id);

    List<EmployeeRole> findAllByRoleId(long id);
}