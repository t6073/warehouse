package pl.oskarskalski.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.oskarskalski.warehouse.model.Item;

import java.util.List;
import java.util.Set;

@Repository
public interface ItemRepository extends JpaRepository<Item, String> {
    List<Item> findAllByItemId(String uuid);
    List<Item> findAllByLocationIdAndItemId(String locationId, String itemId);

    @Query(value = "SELECT DISTINCT item_id FROM item", nativeQuery = true)
    String[] findAllItemIds2();
}
