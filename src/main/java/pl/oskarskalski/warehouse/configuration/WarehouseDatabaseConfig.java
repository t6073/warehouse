package pl.oskarskalski.warehouse.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.oskarskalski.database.configuration.DataSourceConfig;

import javax.sql.DataSource;


@Configuration
public class WarehouseDatabaseConfig {
    @Autowired
    private WarehouseDatabaseSettings dbSettings;

    @Bean
    public DataSource getDataSource() {
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        return dataSourceConfig.getDataSource(dbSettings);
    }
}
