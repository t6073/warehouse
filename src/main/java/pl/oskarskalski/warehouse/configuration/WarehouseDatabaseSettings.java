package pl.oskarskalski.warehouse.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import pl.oskarskalski.database.configuration.DbSettings;

@Configuration
@ConfigurationProperties("db")
public class WarehouseDatabaseSettings extends DbSettings {
}
