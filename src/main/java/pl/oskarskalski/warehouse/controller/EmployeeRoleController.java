package pl.oskarskalski.warehouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.oskarskalski.warehouse.model.EmployeeRole;
import pl.oskarskalski.warehouse.service.EmployeeRoleService;

import java.util.List;

@RestController
public class EmployeeRoleController {
    private final EmployeeRoleService employeeRoleService;

    @Autowired
    public EmployeeRoleController(EmployeeRoleService employeeRoleService) {
        this.employeeRoleService = employeeRoleService;
    }

    @PostMapping("/employee/role")
    public void addEmployeeRole(EmployeeRole employeeRole){
        employeeRoleService.addEmployeeRole(employeeRole);
    }

    @GetMapping("/employee/{employeeId}")
    public List<EmployeeRole> getAllEmployeeRolesByEmployeeId(@PathVariable long employeeId){
        return employeeRoleService.getEmployeeRolesByEmployeeId(employeeId);
    }


    @GetMapping("/employee/role/{roleId}")
    public List<EmployeeRole> getAllEmployeeRolesByRoleId(@PathVariable long roleId){
        return employeeRoleService.getEmployeeRolesByRoleId(roleId);
    }
}
