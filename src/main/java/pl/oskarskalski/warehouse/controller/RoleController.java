package pl.oskarskalski.warehouse.controller;

import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.oskarskalski.warehouse.model.Role;
import pl.oskarskalski.warehouse.service.RoleService;

import java.util.List;

@RestController
public class RoleController {

    private final RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @PostMapping("/role")
    public void addRole(Role role) {
        roleService.addRole(role);
    }

    @GetMapping("/role/all")
    public List<Role> getAllRoles() {
        return roleService.getRoles();
    }

    @GetMapping("/role/{roleId}")
    public Role getRoleById(@PathVariable long roleId) {
        return roleService.getRoleById(roleId);
    }
}
