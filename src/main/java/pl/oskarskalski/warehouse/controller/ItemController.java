package pl.oskarskalski.warehouse.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.oskarskalski.warehouse.model.Item;
import pl.oskarskalski.warehouse.service.ItemService;

import java.util.List;

@RestController
public class ItemController {
    private final ItemService itemService;

    @Autowired
    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }

    @Operation(summary = "Get an item by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the item",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Item.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Item not found",
                    content = @Content)})
    @GetMapping("/item/{id}")
    public Item getItemById(@PathVariable String id) {
        return itemService.getItemById(id);
    }

    @Operation(summary = "Add item to the database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successfully added item to the database"),
            @ApiResponse(responseCode = "400", description = "Invalid data",
                    content = @Content)})
    @PostMapping("/item/")
    public void addItem(@RequestBody Item item) {
        itemService.addItem(item);
    }

    @Operation(summary = "Soft delete an item in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully soft deleted item in the database"),
            @ApiResponse(responseCode = "400", description = "Invalid location/item id",
                    content = @Content)})
    @DeleteMapping("/item/{locationId}/{itemId}")
    public void addItem(@PathVariable("locationId") String locationId, @PathVariable("itemId") String itemId) {
        itemService.softDeleteItemById(locationId, itemId);
    }

    @Operation(summary = "Get items by query string")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found items",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = List.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid items' id",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Items not found",
                    content = @Content)})
    @GetMapping("/items")
    public List<List<Item>> getItemsByItemId(@RequestParam(required = false) String itemsId) {
        return itemService.getItemsByItemsId(itemsId);
    }

    @GetMapping("/items/qwe")
    public ResponseEntity<String> test() {
        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body("Forbidden");
    }
}
