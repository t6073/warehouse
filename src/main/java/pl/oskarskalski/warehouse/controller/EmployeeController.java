package pl.oskarskalski.warehouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.oskarskalski.security.configuration.JwtConfiguration;
import pl.oskarskalski.security.model.UsernameAndPasswordAuthenticationRequest;
import pl.oskarskalski.warehouse.model.EmployeeRequest;
import pl.oskarskalski.warehouse.service.EmployeeService;

@RestController
public class EmployeeController {
    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping("/login")
    public ResponseEntity<String> test(@RequestBody UsernameAndPasswordAuthenticationRequest usernameAndPasswordAuthenticationRequest){
        return employeeService.signInTheEmployee(usernameAndPasswordAuthenticationRequest);
    }
}
