FROM openjdk:11
WORKDIR .
COPY . .
CMD ["java", "-jar","./target/warehouse-0.0.1-SNAPSHOT.jar"]